# Trabajo Practico 1

## Objetivos

El objetivo del trabajo práctico es implementar una aplicación para jugar al siguiente juego.

Se presenta al usuario una grilla de 4 × 4, con números enteros asociados a las filas y las columnas. El jugador debe adivinar que numero poner en cada casillero de la grilla, de modo tal que la suma de los números de cada fila sea igual al valor dado, y que la suma de los números de cada columna sea igual al valor dado. Por ejemplo, la Figura 1 muestra a la izquierda una grilla que se puede presentar al usuario al inicio del juego, y a la derecha el juego resuelto exitosamente.

Para conseguir el objetivo, el grupo diseñó una solución en Java con las siguientes clases, **Controlador, Interfaz, LogicaModelo, Menú y Vista.**

En las siguientes secciones se explica el funcionamiento de cada una y los métodos utilizados para alcanzar dicho objetivo.

## Clases, Métodos y Comportamientos.

### Principal.java

La clase principal es el *main* del proyecto, la misma tendrá la función de iniciar el juego y lo hará haciendo un llamado a la clase **Menu.java**.

La estructura de la misma es la siguiente.

```java
package main;
import interfaz.Menu;

public class Principal {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.setVisible(true);  
    }

}
```

### Controlador.java

La clase *Controlador.java*, es quien se encargará de las operaciones realizadas por el usuario para saber si completó o no el juego.

Esta clase cuenta con un constructor y la misma implementará los métodos de *ActionListener, MouseListener* y sus respectivos métodos.

```java
public class Controlador implements ActionListener, MouseListener {
```

El constructor recibe un objeto de tipo **Vista** y otro de tipo **LogicaModelo**, anteriormente dichos Objetos son inicializados de forma Global y privados, y a continuación se hará la asignación de los *Listeners* de *Action* y *Mouse*, para que al momento que el usuario decida tomar alguna decisión, la solución ejecute la función de acuerdo al pedido por Teclado o Mouse.

```java
		private Vista vista;
	private LogicaModelo modelo;

	public Controlador(Vista vista, LogicaModelo modelo) {

		this.vista = vista;
		this.modelo = modelo;
		this.modelo.setGridSize(this.vista.getGrid());
		this.vista.btnIntentar.addMouseListener(this);
		this.vista.btnLimpiar.addMouseListener(this);
		this.vista.btnReiniciar.addMouseListener(this);
		for (int i = 0; i < this.vista.getGridCells().length; i++) {
			for (int j = 0; j < this.vista.getGridCells()[0].length; j++) {
				this.vista.getGridCells()[i][j].addActionListener(this);
			}
		}
	}
```

El método de *ActionListener **public void actionPerformed(ActionEvent e)***  la acción que realizará es la de verificar si el usuario completó de forma correcta el juego o no, a su vez realizará 2 operaciones más; si el usuario ingresó alguna letra en vez de un número, el mismo será removido y esa casilla mostrará el número 0 y por otro lado si el usuario no ingrese los números correctos para completar el juego, mostrará un mensaje de *Seguir Intentando.* Para el caso que el usuario ingrese los números correctos y las sumas tanto de Fila y Columna son correctas, mostrará una ventana felicitando al usuario.

```java
@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		boolean resultado = modelo.juegoResuelto(this.vista.getGridCells(), 
				this.vista.getSumaFilasEsperada(),
				this.vista.getSumaColumnasEsperada());

		this.vista.pintarGrilla(this.modelo.getAPintar(), this.vista.getGridCells());

		if (this.modelo.contieneLetra())
			this.vista.limpiarLetras(this.vista.getGridCells());

		if (resultado) {
			System.out.println("Bien hecho!!");
			this.vista.ganador();
		} else {
			System.out.println("Sigue intentando");
			this.vista.intentoFallido();
		}
	}
```

De los métodos *MouseListener*, el único que se utiliza en la solución es el de ***mouseClicked***, y las acciones que realiza son:

- Si el usuario hace clic en el botón *Intentar*, las acciones a realizar con las misma que ***actionPerformed.***
- Si el usuario decide Limpiar las grillas de Filas y Columnas deberá hacer clic en el botón *Limpiar.*
- Por último, si el usuario hace clic en el botón Reiniciar, el nivel se reiniciará, así como el tiempo transcurrido y los totales de Fila y Columna serán reemplazados por nuevos números.

```java
@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == vista.btnLimpiar) {
			System.out.println("LIMPIAR CONTROLADOR");
			this.vista.limpiarPanel(this.vista.getGridCells());
		}

		if (e.getSource() == vista.btnIntentar) {
			System.out.println("Intentar CONTROLADOR");

			boolean resultado = modelo.juegoResuelto(this.vista.getGridCells(), 
					this.vista.getSumaFilasEsperada(),
					this.vista.getSumaColumnasEsperada());

			this.vista.pintarGrilla(this.modelo.getAPintar(), this.vista.getGridCells());

			if (this.modelo.contieneLetra())
				this.vista.limpiarLetras(this.vista.getGridCells());

			if (resultado) {
				System.out.println("Bien hecho!!");
				this.vista.ganador();
			} else {
				System.out.println("Sigue intentando");
				this.vista.intentoFallido();
			}

		}
		if (e.getSource() == vista.btnReiniciar) {
			System.out.println("REINICIAR CONTROLADOR");
			this.vista.reiniciarNivel();
		}
	}
```

Los métodos abstractos de ***mousePressed, mouseReleased, mouseEntered*** y ***mouseExited***, no fueron configurados para realizar ninguna acción, si están presentes en la clase Controlador.java porque esta misma implementa a la clase Abstracta *MouseListener* y es obligatorio hacer un @Override de los mismos.

### LogicaModelo.java

La clase *LogicaModelo.java*, se va a encargar de la lógica del juego, mediante la misma podemos saber si el valor ingresado es un número o una letra, si el usuario completo o ganó el juego y si alguna de las celdas ingresadas es correcta y de esta manera se pintarán de verde para ayudar al usuario.

Esta clase no dispone de un Constructor, pero si contiene cinco métodos que se utilizaran en la clase ***Controlador.java**,*  y ellos son:

```java
public boolean juegoResuelto(JTextField[][] gridCells, int sumaEsperadaFilas, 
																									int[] sumaEsperadaColumnas)
```

- Este método analiza las Filas y las Columnas, para ellos recibe por parámetro un JTextField [][] gridCells que contiene una matriz con los valores ingresados por el usuario, un arreglo de enteros con la suma esperada por Filas y otro arreglo de enteros con la suma esperada por Columnas. En este método se realizan 2 cosas importantes, la primera saber si el juego está resuelto o no, y segundo las celdas que tengan un valor posible o correcto serán pintadas de verde.
    
    Para conocer si el juego esta resultó, el método recorrerá primero por fila y luego por columnas, almacenando sus valores en las variables **sumaFila** y **sumaCol**, y las comparara con los valores recibidos en los arreglos **sumaEsperadaFilas y sumpaEsperadaColumnas,** para conocer si los valores son los esperados se utiliza un acumulador booleano **igualdadTotal.** Por otro lado, este método se va a encargar de pintar o no la celda en caso de que **sumaCol == sumaEsperadaColumnas[i].** 
    
    Por último, el método retorna un booleano **True** si las Filas y Columnas tienen valores cuyas sumas dan los totales esperados o **False** para el caso de que aún no está resuelto el juego.
    

```java
public boolean juegoResuelto(JTextField[][] gridCells, int[] sumaEsperadaFilas, 
																									int[] sumaEsperadaColumnas) {
        int longitud = gridCells.length;
        int longitud2 = gridCells[0].length;
        boolean igualdadTotal = true;
        aPintar = new boolean[gridSize][gridSize];
        
        for (int i = 0; i < gridCells[0].length; i++) {
            int sumaFila = 0;
            int sumaCol = 0;
            
            for (int j = 0; j < longitud; j++) {
                String value = gridCells[i][j].getText();
                if(isNumeric(value)) {
                    sumaFila += Integer.parseInt(value);
                }
            }
            igualdadTotal = igualdadTotal && sumaFila == sumaEsperadaFilas[i];
            
            if(sumaFila == sumaEsperadaFilas[i]) {
                for (int j = 0; j < longitud; j++) {
                    this.aPintar[i][j] = true;
                }
            }else {
                for (int j = 0; j < longitud; j++) {
                    this.aPintar[i][j] = false;
                }
            }           
            
            for (int j = 0; j < longitud2; j++) {
                String value = gridCells[j][i].getText();
                if(isNumeric(value)) {
                    sumaCol += Integer.parseInt(value);
                }
            }
            igualdadTotal = igualdadTotal && sumaCol == sumaEsperadaColumnas[i];
            
            if(sumaCol == sumaEsperadaColumnas[i]) {
                for (int j = 0; j < longitud2; j++) {
                    this.aPintar[j][i] = true;
                }
            }else {
                for (int j = 0; j < longitud2; j++) {
                    this.aPintar[j][i] = false;
                }
            }
        }
        return igualdadTotal;
    }
```

- **private static boolean esNumero(String cadena)**: Este método  se encargará únicamente en validar que la cadena de texto recibida por parámetro es un número, esto ayudará a controlar que el usuario no ingrese letras en las casillas donde se esperan números.

```java
private static boolean esNumero(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException excepcion) {
            contieneLetra = true;
            return false;
        }
    }
```

- **public void setGridSize(int size):** La única acción de este método será asignar un valor entero al tamaño del Grid, el cual se utiliza en el método ya mencionado de ***juegoResuelto()*** para así crear una matriz auxiliar para poder pintar las celdas correctas.

```java
public void setGridSize(int size) {
        this.gridSize = size;
    }
```

- **public boolean[][] getAPintar():** Lo utilizamos para conocer la matriz auxiliar y así saber qué tamaño tiene la misma y que posiciones hay que pintar, la matriz auxiliar se llenará con booleanos.

```java
public boolean[][] getAPintar() {
        return this.aPintar;
    }
```

- **public boolean contieneLetra():** Al igual que los dos anteriores, este método se utiliza para brindar información a la solución para saber qué decisión tomar, en este caso si existe alguna letra en alguna casilla el sistema deberá borrarla, el método para borrar las letras está presente en la clase *Controlador.java.*

```java
public boolean contieneLetra() {
        return this.contieneLetra;
    }
```

### Menu.java

La clase Menu.java, es quien crea una ventana inicial del Juego, mediante la misma el usuario tomará la decisión de qué nivel quiere jugar, los cuales son 3 (tres).

- Nivel Fácil: Una grilla de 4x4.
- Nivel Intermedio: La grilla es de 5x5.
- Nivel Difícil: La grilla es de 6x6.

A su vez, mediante esta clase se enviaran parámetros a las clases **Vista.java**  y **Controlador.java**, para así configurar la grilla en **Vista.java** según el nivel seleccionado, y lo mismo a la clase **Controlador.java**, según el nivel seleccionado, se enviará por parámetro el tamaño del Grid y Grilla preferente del nivel, y en base de esto **Vista.java** realizara el diseño deseado y **Controlador.java** tendrá noción de estas dimensiones de Grid y Grilla y podrá realizar las operaciones necesarias.

Para la selección de nivel se implementaron métodos sobre los botones, para ello se utilizo el metodo nativo **addActionListener(new ActionListener()** propio de la librería *java.awt.Button*. Según la selección del nivel el juego tendrá un diseño particular, como se mencionó la grilla puede ser 4x4 (Facil), 5x5 (Intermedio) o 6x6 para el nivel difícil. Así como el diseño de la ventana y las acciones de los botones que esta incluye, están dentro del método **private void crearVentana()**, aquí se define el diseño de la ventana en sí, el diseño de cada botón, el texto de cada botón y las acciones que se realizan para cada uno. Según la decisión que tome el usuario, el código hará llamado a la clase **Vista.java**  y los parámetros para cada nivel, otro llamado la clase **Controlado.java**  también con sus respectivos parámetros.

El diseño del método **crearVentana()**  es el siguiente.

```java
{
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 236, 410);
        setTitle("Trabajo Practico 1: Juegos aritmeticos");
        gridPanel = new JPanel(new GridLayout(4, 4));
        getContentPane().add(gridPanel, BorderLayout.CENTER);
        
        Panel panel = new Panel();
        panel.setBackground(new Color(51, 102, 153));
        gridPanel.add(panel);
        
        Canvas canvas = new Canvas();
        panel.add(canvas);
        
        JTextArea txtrSeleccioneUnaDificultad = new JTextArea();
        txtrSeleccioneUnaDificultad.setEditable(false);
        txtrSeleccioneUnaDificultad.setBackground(new Color(51, 102, 153));
        txtrSeleccioneUnaDificultad.setWrapStyleWord(true);
        txtrSeleccioneUnaDificultad.setRows(2);
        txtrSeleccioneUnaDificultad.setColumns(1);
        txtrSeleccioneUnaDificultad.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 13));
        txtrSeleccioneUnaDificultad.setTabSize(10);
        txtrSeleccioneUnaDificultad.setText("\r\nSeleccione una dificultad");
        panel.add(txtrSeleccioneUnaDificultad);
        
        Button botonn_Facil = new Button("Facil");
        botonn_Facil.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                //Va a crear el GRID de 4x4
                Vista vista = new Vista(4);
                System.out.println("Selecciono Facil");
                Controlador controlador = new Controlador(vista, modelo);
                vista.setVisible(true);
                setVisible(false);
                
                
            }
        });
        gridPanel.add(botonn_Facil);
        
        Button boton_Interm = new Button("Intermedio");
        boton_Interm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                //Va a crear el GRID de 5x5
                Vista vista = new Vista(5);
                System.out.println("Selecciono Intermedio");
                Controlador controlador = new Controlador(vista, modelo);
                vista.setVisible(true);
                setVisible(false);

            }
        });
        gridPanel.add(boton_Interm);
        
        Button boton_Dificil = new Button("Dificil");
        boton_Dificil.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                //Va a crear el GRID de 6x6
                Vista vista = new Vista(6);
                System.out.println("Selecciono Dificil");
                Controlador controlador = new Controlador(vista, modelo);
                vista.setVisible(true);
                setVisible(false);

            }
        });
        gridPanel.add(boton_Dificil);
      
    }
```

Por último, la clase Menu.java dispone de dos métodos más:

- Su constructor Menu(), el cual se encargará únicamente de iniciar la ventana
    
    ```java
    public Menu() {
            setAlwaysOnTop(true);
            iniciar();                
        }
    ```
    
- Y el método iniciar() que es ejecutado desde el constructor de la clase y él mismo hará uso del método mencionado **crearVentana().**
    
    ```java
    private void iniciar() {
            crearVentana();
        }
    ```
    

### Vista.java

La clase *Vista.java*, se va a encargar de ser la interfaz visual del juego, donde el jugador va a observar la grilla del tamaño elegido en menu y va a ir ingresando los números acorde a las sumas esperadas generadas por esta misma clase, en esta ademas se veran los mensajes de texto que le indicarán al usuario si su intento es fallido o, en caso de ganar el juego, felicitarlo y mostrarle el tiempo que le tomó resolver el mismo.

Como propiedades la clase vista contiene:

```java
private int GRID_SIZE = 0; //es el tamaño de la grilla.
private JPanel gridPanel; //es el panel que contendrá a la grilla.
private JTextField[][] gridCells; //matriz de JTextField que representa a la grilla.
private JLabel[] filaSumas; //suma que se espera que de una fila.
private JLabel[] columnaSumas; //suma que se espera que de una columna.
private JPanel filasSumPanel; //JPanel que contiene la suma de las filas esperadas.
private JPanel columnaSumPanel; //JPanel que contiene la suma de las columnas esperadas.
private JPanel botonesPanel; //JPanel que contendra los botones de la interfaz.
private int[] sumaEsperadaCol; //Array que contiene la suma esperada de cada columna.
private int[] sumaEsperadaFila; //Array que contiene la suma esperada de cada fila.
private int sumaColumna; //Total de la suma de todas las columnas.
private int totalColumnas; //Total de la suma de todas las filas.
public JButton btnIntentar = new JButton("Intentar"); //Botón para ver si el intento 
//es correcto.
public JButton btnLimpiar = new JButton("Limpiar"); //Botón para limpiar la grilla.
public JButton btnReiniciar = new JButton("Reiniciar"); //Botón para reiniciar el juego.
private JLabel tiempoLabel = new JLabel("Tiempo: 0 segundos"); //Contador de tiempo.
private String[][] mensajesIntento = new String[3][4]; //Contiene los mensajes que el 
//juego le va a mostrar al usuario en cada intento, dependiendo de la dificultad son más 
//amigables u hostiles.
private String[] mensajesFacil = new String[4];
private String[] mensajesMedio = new String[4];
private String[] mensajesDificil = new String[4];
private long inicio; //Inicia el tiempo.
private long fin; //Termina el tiempo.
private int segundos; //Es el resultado de (double) ((fin - inicio)/1000)
private int minutos; //Es el resultado de segundos/60, si este es mayor a 60 o igual.
private double tiempoRecord; //Muestra el mejor tiempo obtenido.
private Timer timer; //Muestra gráficamente el tiempo al usuario en el juego.
```

Constructor: Setea el tamaño de la grilla que es pasado por parámetros y llama al método iniciar().

```java
public Vista(int gridSize) {
		this.setGrid(gridSize);
		iniciar();
	}
```

Métodos:

- Iniciar(): Es el encargado de llamar a los métodos de la clase responsables de iniciar cada componente necesario de la clase vista para su visualización. Ademas es quien setea el tiempo de inicio y da comienzo al timer.
    
    ```java
    private void iniciar() {
    		
    		crearVentanaJuego();	
    		crearCasillas();
    		generarSumasFilaColumna();		
    		generarMensajesUsuario();	
    		chequearTotales();
    		inicio = System.currentTimeMillis(); //Comienza a contar el tiempo
    		timer.start(); 
    	}
    ```
    
- crearVentanaJuego(): Se encarga de crear la ventana del juego, seteando sus dimensiones el titulo, inicializando y agregando el panel; las filas y columnas de las sumas esperadas; y los botones de la interfaz.
    
    ```java
    private void crearVentanaJuego() {
    		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		setSize(800, 550);
    		setTitle("Trabajo Practico 1: Juegos aritmeticos");
    		gridPanel = new JPanel(new GridLayout(this.getGrid(), this.getGrid()));
    		getContentPane().add(gridPanel, BorderLayout.CENTER);	
    		
    		this.rowSumPanel = new JPanel(new GridLayout(this.getGrid(), 1));
    		this.colSumPanel = new JPanel(new GridLayout(1, this.getGrid()));
    		filaSumas = new JLabel[this.getGrid()]; 
    		// Muestra el valor esperado por file
    		columnaSumas = new JLabel[this.getGrid()]; 
    		// Muestra el valor esperado por columna
    		sumaEsperadaCol = new int[this.getGrid()];
    		sumaEsperadaFila = new int[this.getGrid()];
    		for (int i = 0; i < this.getGrid(); i++) {
    			filaSumas[i] = new JLabel("0");
    			columnaSumas[i] = new JLabel("0");
    			rowSumPanel.add(filaSumas[i]);
    			colSumPanel.add(columnaSumas[i]);
    			}	
    		
    		this.botonesPanel = new JPanel(new GridLayout(0, 5));
    		
    		getContentPane().add(rowSumPanel, BorderLayout.EAST);
    		getContentPane().add(colSumPanel, BorderLayout.SOUTH);
    		getContentPane().add(botonesPanel, BorderLayout.NORTH);
    
    		this.botonesPanel.add(btnIntentar);
    		this.botonesPanel.add(btnLimpiar);
    		this.botonesPanel.add(btnReiniciar);
    		tiempoLabel.setHorizontalAlignment(SwingConstants.TRAILING);
    		this.botonesPanel.add(tiempoLabel);
    		timer = new Timer(1000, this);
    
    		btnIntentar.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent e) {
    				btnIntentar.setActionCommand("Intentar");
    
    			}
    		});
    		btnLimpiar.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent e) {
    				btnLimpiar.setActionCommand("Limpiar");
    
    			}
    		});
    		btnReiniciar.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent e) {
    				btnReiniciar.setActionCommand("Reiniciar");
    
    			}
    		});
    		
    		
    	}
    ```
    
- crearCasillas(): Metodo encargado de crear las casillas JTextField donde el usuario ingresara los numeros intentando llegar a la suma estipulada.
    
    ```java
    private void crearCasillas() {
    		gridCells = new JTextField[this.getGrid()][this.getGrid()];
    		for (int i = 0; i < this.getGrid(); i++) {
    			for (int j = 0; j < this.getGrid(); j++) {
    				gridCells[i][j] = new JTextField();
    				gridCells[i][j].setText("0");
    				gridPanel.add(gridCells[i][j]);
    			}
    		}
    		
    	}
    ```
    
- generarSumasFilaColumna():  Este metodo llama a generarArregloRandom y generarArregloTotalSuma, para generar el arreglo de las sumas esperadas de las filas, y el arreglo de las sumas esperadas de las columnas, luego con esto se encarga de agregarlos a la vista actualizando los JLabel.
    
    ```java
    private void generarSumasFilaColumna() {
    
    		int[] arregloColumna = generarArregloRandom(this.getGrid());
    		int[] arregloFila = generarArregloTotalSuma(arregloColumna);
    			
    		for (int col = 0; col < this.getGrid(); col++) {
    			sumaEsperadaCol[col] = arregloColumna[col];
    			totalColumnas += sumaEsperadaCol[col];
    		}
    		for (int fil = 0; fil < this.getGrid(); fil++) {
    			sumaEsperadaFila[fil] = arregloFila[fil];
    			totalFilas += sumaEsperadaFila[fil];
    		}
    		
    		// Actualiza las etiquetas de las sumas
    		for (int i = 0; i < this.getGrid(); i++) {
    			filaSumas[i].setText(Integer.toString(sumaEsperadaFila[i]));
    			columnaSumas[i].setText(Integer.toString(sumaEsperadaCol[i]));
    		}
    	
    	}
    ```
    
- generarArregloRandom(int length): Genera un arreglo de numeros random del tamaño de la grilla de la vista, este se usa para la suma esperada de las columnas.
    
    ```java
    private int[] generarArregloRandom(int length) { 
    //Genera el arreglo a usar en columnas
    		Random random = new Random();
    		int[] arr = new int[length];
    		for (int i = 0; i < length; i++) {
    			arr[i] = random.nextInt(9) + 7;
    		}
    		return arr;
    	}
    ```
    
- generarArregloTotalSuma(int[] arr): Genera un arreglo de numeros random del tamaño de la grilla de la vista, este se usa para la suma esperada de las filas. Este tiene la particularidad que los elementos generados deben sumar lo mismo que la suma de los elementos del arreglo de numeros generado para la suma esperada de las columnas, ya que de esta manera nos aseguramos de que el juego efectivamente pueda completarse.
    
    ```java
    private int[] generarArregloTotalSuma(int[] arr) {
    //Se genera un arreglo a usar Filas, arreglo columnas apsa por paremetro
       	 Random random = new Random();
       	 int[] arr2 = new int[arr.length];
       	 int sum1 = Arrays.stream(arr).sum();
       	 int sum2 = 0;
       	 int indiceMayor = 0;
       	 for (int i = 0; i < arr2.length - 1; i++) {
       		 arr2[i] = random.nextInt(15-this.getGrid()+1) + this.getGrid();
       		 if(arr2[i] > arr2[indiceMayor]) indiceMayor = i;
       		 sum2 += arr2[i];
       	 }
       	 if(sum1 - sum2 < 1) {
       		 arr2[indiceMayor] -= (sum1 - sum2) + 1;
       		 arr2[arr2.length - 1] = 1;
       	 }else {
       		 arr2[arr2.length - 1] = sum1 - sum2;
       	 }
       	 
       	 return arr2;
        }
    ```
    
- generarMensajesUsuario(): Matrix de mensajes que seran mostrados al usuario dependiendo la dificultad elegida, siendo cada vez más hostil cuando la dificultad tiende a dificil.
    
    ```java
    private void generarMensajesUsuario() {		
    		/*
    		 * Agarra mensaje aleatorio entre todos los mensajes, dependiendo la dificultad
    		 * elegida, y se lo muestra al usuario. Notar que el grid_size se ve afectado
    		 * por la dificultad. Creacion de mensajes dependiendo la dificultad elegida por
    		 * el usuario
    		 */
    		
    		// Facil:
    		mensajesFacil[0] = "Aún no, sigue intentando!";
    		mensajesFacil[1] = "Cerca, pero no.";
    		mensajesFacil[2] = "Mmmm casi.";
    		mensajesFacil[3] = "Dale, vos podes.";
    		// Medio
    		mensajesMedio[0] = "Todavia no.";
    		mensajesMedio[1] = "Mmm no.";
    		mensajesMedio[2] = "Va queriendo.";
    		mensajesMedio[3] = "Segui intentando.";
    		// Dificil
    		mensajesDificil[0] = "Incorrecto, eso que no es tan dificil.";
    		mensajesDificil[1] = "Mmm no, para nada.";
    		mensajesDificil[2] = "Deberias probar jugando a otra cosa, esto no es para vos.";
    		mensajesDificil[3] = "No, no y no.";
    
    		mensajesIntento[0] = mensajesFacil;
    		mensajesIntento[1] = mensajesMedio;
    		mensajesIntento[2] = mensajesDificil;
    	}
    ```
    
- chequearTotales(): Chequea que ningun elemento de las sumas esperadas tanto de las filas como de las columnas sean menor al largo de la matriz.
    
    ```java
    private void chequearTotales() {
    		// TODO Auto-generated method stub
    		for (int i = 0; i < this.getGrid(); i++) {
    			if (sumaEsperadaCol[i] < getGrid() || sumaEsperadaFila[i] < getGrid())
    			{
    				iniciar();
    			}
    			
    		}
    	}
    ```
    
- ganador(): Este metodo se ejecuta una vez que el jugador completa la grilla con los valores que dan la suma esperada, se muestra un mensaje felicitando al usuario y además el tiempo que este tardo en completar el juego.
    
    ```java
    public void ganador() {
    		timer.stop();
    		fin = System.currentTimeMillis();
    		double tiempo = (double) ((fin - inicio)/1000);
    		if(tiempoRecord > tiempo) tiempoRecord = tiempo;
    		setearTiempo(tiempo);
    		JOptionPane.showMessageDialog(this, "¡Felicidades has ganado! Tiempo tardado: " 
    																					+ minutos + ":" + segundos + " minutos");
    		setVisible(false);
    		menu.setVisible(true);
    		
    	}
    ```
    
- intentoFallido(): Se muestra un mensaje al jugador avisandole de que todavia le queda camino por delante para terminar el juego, para esto se elige un mensaje aleatorio de la matriz de mensajes, verificando además en que dificultad se encuentra el juego.
    
    ```java
    public void intentoFallido() {
    		int numeroAleatorio = (int) (Math.random() * 4);
    		int dificultad = this.GRID_SIZE == 4 ? 0 : this.GRID_SIZE == 5 ? 1 : 2;
    		String mensajeAleatorio = this.mensajesIntento[dificultad][numeroAleatorio];
    		JOptionPane.showMessageDialog(this, mensajeAleatorio);
    	}
    ```
    
- setearTiempo(double tiempo): setea los minutos y los segundos de la partida actual, se lo llama cuando el jugador gana la partida.
    
    ```java
    private void setearTiempo(double tiempo) {
    		minutos = 0;
    		while(tiempo > 60) {
    			tiempo -= 60;
    			minutos += 1;
    		}
    		segundos = (int) tiempo;
    	}
    ```
    
- pintarGrilla(boolean[][] aPintar, JTextField[][] gridCells): Pinta los JTextField de la grilla que tengan un valor que coincida con la suma esperada, esto lo hace recorriendo una matriz de booleanos que en caso de tener true en [x][y] la matriz de booleanos, el valor ingresado por el usuario en la posicion [x][y] de la matriz de la grilla cumple con la condicion de la suma (este arreglo lo genera LogicaModelo), al mismo tiempo se “despinta” a los que no cumplan con estas condiciones.
    
    ```java
    public void pintarGrilla(boolean[][] aPintar, JTextField[][] gridCells) {
    		for (int i = 0; i < this.getGrid(); i++) {
    			for (int j = 0; j < this.getGrid(); j++) {
    				if (aPintar[i][j]) {
    					gridCells[i][j].setBackground(getForeground().GREEN);
    
    				} else {
    					gridCells[i][j].setBackground(getForeground().white);
    				}
    
    				gridPanel.add(gridCells[i][j]);
    			}
    		}
    	}
    ```
    
- limpiarPanel(JTextField[][] gridCells): Se setean todos los JTextField de la grilla en 0 y se los pinta de blanco.
    
    ```java
    public void limpiarPanel(JTextField[][] gridCells) {
    		for (int i = 0; i < this.getGrid(); i++) {
    			for (int j = 0; j < this.getGrid(); j++) {
    				gridCells[i][j].setText("0");
    				gridCells[i][j].setBackground(getForeground().white);
    				gridPanel.add(gridCells[i][j]);
    			}
    		}
    	}
    ```
    
- limpiarLetras(JTextField[][] gridCells): Se verifican los JTextField que contienen una letra y en caso de que así sea se convierten en 0.
    
    ```java
    public void limpiarLetras(JTextField[][] gridCells) {
    		for (int i = 0; i < this.getGrid(); i++) {
    			for (int j = 0; j < this.getGrid(); j++) {
    				boolean esLetra = esLetra(gridCells[i][j].getText());
    				if (esLetra) {
    					gridCells[i][j].setText("0");
    					gridCells[i][j].setBackground(getForeground().white);
    				}
    				gridPanel.add(gridCells[i][j]);
    			}
    		}
    	}
    ```
    
- esLetra(String cadena): Verifica si la cadena ingresada contiene una letra, es decir, no es un numero.
    
    ```java
    private static boolean esLetra(String cadena) {
    		try {
    			Integer.parseInt(cadena);
    			return false;
    		} catch (NumberFormatException excepcion) {
    			return true;
    		}
    	}
    ```
    
- reiniciarNivel(): Vuelve a iniciar el juego.
    
    ```java
    public void reiniciarNivel() {
    		iniciar();
    	}
    ```
    
- setGrid(): Setea el tamaño de la grilla.
    
    ```java
    public void setGrid(int valor) {
    		this.GRID_SIZE = valor;
    	}
    ```
    
- getGrid(): retorna el tamaño de la grilla.
    
    ```java
    public int getGrid() {
    		return GRID_SIZE;
    	}
    ```
    
- getSumaFilasEsperada(): Retorna el arreglo con la suma, de las filas, esperada.
    
    ```java
    public int[] getSumaFilasEsperada() {
    		return sumaEsperadaFila;
    	}
    ```
    
- getSumaColumnasEsperada(): Retorna el arreglo con la suma, de las columnas, esperada.
    
    ```java
    public int[] getSumaColumnasEsperada() {
    		return sumaEsperadaCol;
    	}
    ```
    
- getGridCells(): Retorna la matriz(grilla) actual.
    
    ```java
    public JTextField[][] getGridCells() {
    		return gridCells;
    	}
    ```
    
- actionPerformed(): Se encarga de mostrar los segundos en tiempo real en la app.
    
    ```java
    @Override
    	public void actionPerformed(ActionEvent arg0) {
    		// TODO Auto-generated method stub
    		long tiempoTranscurrido = (System.currentTimeMillis() - inicio) / 1000; 
    		// Divido por 1000 para convertir milisegundos a segundos
            tiempoLabel.setText("Tiempo: " + tiempoTranscurrido + " segundos");
    	}
    ```
    

## Participantes:

- Fernando Abrile
- Tomás Ledesma

Programación 3 - Comisión 1, Universidad Nacional de General Sarmiento.