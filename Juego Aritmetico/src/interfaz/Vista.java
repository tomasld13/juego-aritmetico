package interfaz;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.event.*;
import java.util.Random;
import javax.swing.Timer;

import controlador.Controlador;

public class Vista extends JFrame implements ActionListener {

	// Tamaño de la grilla
	public int GRID_SIZE = 0;
	public JPanel gridPanel;
	public JTextField[][] gridCells;

	// Sumas de cada fila y columna
	public JLabel[] filaSumas;
	public JLabel[] columnaSumas;
	private JPanel rowSumPanel;
	private JPanel colSumPanel;
	private JPanel botonesPanel;

	// Valor de la suma esperada
	public int[] sumaEsperadaCol;
	public int[] sumaEsperadaFila;
	public int sumaColumna;
	public int totalColumnas;
	public int totalFilas;
	public Random rnd = new Random();

	public JButton btnIntentar = new JButton("Intentar");
	public JButton btnLimpiar = new JButton("Limpiar");
	public JButton btnReiniciar = new JButton("Reiniciar");
	private JLabel tiempoLabel = new JLabel("Tiempo: 0 segundos");

	// Mensajes para respuestas del intento.
	public String[][] mensajesIntento = new String[3][4];
	public String[] mensajesFacil = new String[4];
	public String[] mensajesMedio = new String[4];
	public String[] mensajesDificil = new String[4];


	public Vista(int gridSize) {
		this.setGrid(gridSize);
		iniciar();

	}

	public void iniciar() {

		crearVentanaJuego();
		Controlador.crearCasillas(this);
		Controlador.generarSumasFilaColumna(this);
		Controlador.generarMensajesUsuario(this);
		Controlador.chequearTotales(this);
		Controlador.inicio = System.currentTimeMillis();
		Controlador.timer.start();
	}


	private void crearVentanaJuego() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 550);
		setTitle("Trabajo Practico 1: Juegos aritmeticos");
		gridPanel = new JPanel(new GridLayout(this.getGrid(), this.getGrid()));
		getContentPane().add(gridPanel, BorderLayout.CENTER);

		this.rowSumPanel = new JPanel(new GridLayout(this.getGrid(), 1));
		this.colSumPanel = new JPanel(new GridLayout(1, this.getGrid()));
		filaSumas = new JLabel[this.getGrid()]; // Muestra el valor esperado por file
		columnaSumas = new JLabel[this.getGrid()]; // Muestra el valor esperado por columna
		sumaEsperadaCol = new int[this.getGrid()];
		sumaEsperadaFila = new int[this.getGrid()];
		for (int i = 0; i < this.getGrid(); i++) {
			filaSumas[i] = new JLabel("0");
			columnaSumas[i] = new JLabel("0");
			rowSumPanel.add(filaSumas[i]);
			colSumPanel.add(columnaSumas[i]);
		}

		this.botonesPanel = new JPanel(new GridLayout(0, 5));
		getContentPane().add(rowSumPanel, BorderLayout.EAST);
		getContentPane().add(colSumPanel, BorderLayout.SOUTH);
		getContentPane().add(botonesPanel, BorderLayout.NORTH);

		this.botonesPanel.add(btnIntentar);
		this.botonesPanel.add(btnLimpiar);
		this.botonesPanel.add(btnReiniciar);
		tiempoLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		this.botonesPanel.add(tiempoLabel);
		Controlador.timer = new Timer(1000, this);

	}
	public void setGrid(int valor) {
		this.GRID_SIZE = valor;
	}

	public int getGrid() {
		return GRID_SIZE;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		long tiempoTranscurrido = (System.currentTimeMillis() - Controlador.inicio) / 1000; // Divido por 1000 para convertir
																				// milisegundos a segundos
		tiempoLabel.setText("Tiempo: " + tiempoTranscurrido + " segundos");
	}

}
