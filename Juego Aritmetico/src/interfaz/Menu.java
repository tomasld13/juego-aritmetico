package interfaz;

import modelo.LogicaModelo;
import controlador.Controlador;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.event.*;
import java.awt.Button;
import java.awt.Panel;
import java.awt.Canvas;
import java.awt.Color;
import javax.swing.JTextArea;
import java.awt.Font;

public class Menu extends JFrame {
	
	// Panel de la grilla
    private JPanel gridPanel;
    // Casillas de la grilla
    public JTextField[][] gridCells;
	LogicaModelo modelo = new LogicaModelo();		

	public Menu() {
		setAlwaysOnTop(true);
		iniciar();				
	}
	private void iniciar() {
		crearVentana();
	}
	
	private void crearVentana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 236, 410);
		setTitle("Trabajo Practico 1: Juegos aritmeticos");
		gridPanel = new JPanel(new GridLayout(4, 4));
        getContentPane().add(gridPanel, BorderLayout.CENTER);
        
        Panel panel = new Panel();
        panel.setBackground(new Color(51, 102, 153));
        gridPanel.add(panel);
        
        Canvas canvas = new Canvas();
        panel.add(canvas);
        
        JTextArea txtrSeleccioneUnaDificultad = new JTextArea();
        txtrSeleccioneUnaDificultad.setEditable(false);
        txtrSeleccioneUnaDificultad.setBackground(new Color(51, 102, 153));
        txtrSeleccioneUnaDificultad.setWrapStyleWord(true);
        txtrSeleccioneUnaDificultad.setRows(2);
        txtrSeleccioneUnaDificultad.setColumns(1);
        txtrSeleccioneUnaDificultad.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 13));
        txtrSeleccioneUnaDificultad.setTabSize(10);
        txtrSeleccioneUnaDificultad.setText("\r\nSeleccione una dificultad");
        panel.add(txtrSeleccioneUnaDificultad);
        
        Button botonn_Facil = new Button("Facil");
        botonn_Facil.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		//Va a crear el GRID de 4x4
        		Vista vista = new Vista(4);
        		System.out.println("Selecciono Facil");
        		Controlador controlador = new Controlador(vista, modelo);
        	//	vista = new Vista (controlador, modelo, 4);
        		vista.setVisible(true);
        		setVisible(false);
        		
        		
        	}
        });
        gridPanel.add(botonn_Facil);
        
        Button boton_Interm = new Button("Intermedio");
        boton_Interm.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		//Va a crear el GRID de 5x5
        		Vista vista = new Vista(5);
        		System.out.println("Selecciono Intermedio");
        		Controlador controlador = new Controlador(vista, modelo);
        		vista.setVisible(true);
        		setVisible(false);

        	}
        });
        gridPanel.add(boton_Interm);
        
        Button boton_Dificil = new Button("Dificil");
        boton_Dificil.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		//Va a crear el GRID de 6x6
        		Vista vista = new Vista(6);
        		System.out.println("Selecciono Dificil");
        		Controlador controlador = new Controlador(vista, modelo);
        		vista.setVisible(true);
        		setVisible(false);

        	}
        });
        gridPanel.add(boton_Dificil);
      
	}
	
}
