package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;

import interfaz.Vista;
import modelo.LogicaModelo;
import interfaz.Menu;

public class Controlador implements ActionListener, MouseListener {
	private Vista vista;
	private LogicaModelo modelo;
	public static Timer timer;
	
	public static  long inicio;	
	public static long fin;
	public static int segundos;
	public static int minutos;
	public static double tiempoRecord;

	public Controlador(Vista vista, LogicaModelo modelo) {

		this.vista = vista;
		this.modelo = modelo;
		this.modelo.setGridSize(this.vista.getGrid());
		this.vista.btnIntentar.addMouseListener(this);
		this.vista.btnLimpiar.addMouseListener(this);
		this.vista.btnReiniciar.addMouseListener(this);
		for (int i = 0; i < this.vista.gridCells.length; i++) {
			for (int j = 0; j < this.vista.gridCells[0].length; j++) {
				this.vista.gridCells[i][j].addActionListener(this);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		boolean resultado = modelo.juegoResuelto(this.vista.gridCells, this.vista.sumaEsperadaFila,
				this.vista.sumaEsperadaCol);

		pintarGrilla(this.modelo.getAPintar(), this.vista.gridCells, vista);

		if (this.modelo.contieneLetra())
			limpiarLetras(this.vista.gridCells, vista);

		if (resultado) {
			System.out.println("Bien hecho!!");
			ganador(vista);
		} else {
			System.out.println("Sigue intentando");
			intentoFallido(vista);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == vista.btnLimpiar) {
			System.out.println("LIMPIAR CONTROLADOR");
			limpiarPanel(this.vista.gridCells, vista);
		}

		if (e.getSource() == vista.btnIntentar) {
			System.out.println("Intentar CONTROLADOR");

			boolean resultado = modelo.juegoResuelto(this.vista.gridCells, this.vista.sumaEsperadaFila,
					this.vista.sumaEsperadaCol);

			pintarGrilla(this.modelo.getAPintar(), this.vista.gridCells, vista);

			if (this.modelo.contieneLetra())
				limpiarLetras(this.vista.gridCells, vista);

			if (resultado) {
				System.out.println("Bien hecho!!");
				ganador(vista);
			} else {
				System.out.println("Sigue intentando");
				intentoFallido(vista);
			}

		}
		if (e.getSource() == vista.btnReiniciar) {
			System.out.println("REINICIAR CONTROLADOR");
			reiniciarNivel(vista);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public static void crearCasillas(Vista vista) {
		
		vista.gridCells = new JTextField[vista.getGrid()][vista.getGrid()];
			for (int i = 0; i < vista.getGrid(); i++) {
				for (int j = 0; j < vista.getGrid(); j++) {
					vista.gridCells[i][j] = new JTextField();
					vista.gridCells[i][j].setText("0");
					vista.gridPanel.add(vista.gridCells[i][j]);
				}
			}
			
		}

	public static void generarSumasFilaColumna(Vista vista) {
		int[] arregloColumna = generarArregloRandom(vista.getGrid());
		int[] arregloFila = generarArregloTotalSuma(arregloColumna,vista);
			
		for (int col = 0; col < vista.getGrid(); col++) {
			vista.sumaEsperadaCol[col] = arregloColumna[col];
			vista.totalColumnas += vista.sumaEsperadaCol[col];
		}
		for (int fil = 0; fil < vista.getGrid(); fil++) {
			vista.sumaEsperadaFila[fil] = arregloFila[fil];
			vista.totalFilas += vista.sumaEsperadaFila[fil];
		}
		
		// Actualiza las etiquetas de las sumas
		for (int i = 0; i < vista.getGrid(); i++) {
			vista.filaSumas[i].setText(Integer.toString(vista.sumaEsperadaFila[i]));
			vista.columnaSumas[i].setText(Integer.toString(vista.sumaEsperadaCol[i]));
		}
		
	}
	
	public static int[] generarArregloRandom(int length) { //Genera el arreglo a usar en columnas
		Random random = new Random();
		int[] arr = new int[length];
		for (int i = 0; i < length; i++) {
			arr[i] = random.nextInt(9) + 7;
		}
		return arr;
	}

	public static int[] generarArregloTotalSuma(int[] arr, Vista vista) { //Se genera un arreglo a usar Filas, arreglo columnas apsa por paremetro
	   	 Random random = new Random();
	   	 int[] arr2 = new int[arr.length];
	   	 int sum1 = Arrays.stream(arr).sum();
	   	 int sum2 = 0;
	   	 int indiceMayor = 0;
	   	 for (int i = 0; i < arr2.length - 1; i++) {
	   		 arr2[i] = random.nextInt(15-vista.getGrid()+1) + vista.getGrid();
	   		 if(arr2[i] > arr2[indiceMayor]) indiceMayor = i;
	   		 sum2 += arr2[i];
	   	 }
	   	 if(sum1 - sum2 < 1) {
	   		 arr2[indiceMayor] -= (sum1 - sum2) + 1;
	   		 arr2[arr2.length - 1] = 1;
	   	 }else {
	   		 arr2[arr2.length - 1] = sum1 - sum2;
	   	 }
	   	 
	   	 return arr2;
	    }

	public static void generarMensajesUsuario(Vista vista) {
		/*
		 * Agarra mensaje aleatorio entre todos los mensajes, dependiendo la dificultad
		 * elegida, y se lo muestra al usuario. Notar que el grid_size se ve afectado
		 * por la dificultad. Creacion de mensajes dependiendo la dificultad elegida por
		 * el usuario
		 */
		
		// Facil:
		vista.mensajesFacil[0] = "Aún no, sigue intentando!";
		vista.mensajesFacil[1] = "Cerca, pero no.";
		vista.mensajesFacil[2] = "Mmmm casi.";
		vista.mensajesFacil[3] = "Dale, vos podes.";
		// Medio
		vista.mensajesMedio[0] = "Todavia no.";
		vista.mensajesMedio[1] = "Mmm no.";
		vista.mensajesMedio[2] = "Va queriendo.";
		vista.mensajesMedio[3] = "Segui intentando.";
		// Dificil
		vista.mensajesDificil[0] = "Incorrecto, eso que no es tan dificil.";
		vista.mensajesDificil[1] = "Mmm no, para nada.";
		vista.mensajesDificil[2] = "Deberias probar jugando a otra cosa, esto no es para vos.";
		vista.mensajesDificil[3] = "No, no y no.";

		vista.mensajesIntento[0] = vista.mensajesFacil;
		vista.mensajesIntento[1] = vista.mensajesMedio;
		vista.mensajesIntento[2] = vista.mensajesDificil;
	}

	public static void chequearTotales(Vista vista) {		
			// TODO Auto-generated method stub
			for (int i = 0; i < vista.getGrid(); i++) {
				if (vista.sumaEsperadaCol[i] < vista.getGrid() || vista.sumaEsperadaFila[i] < vista.getGrid()) {
					vista.iniciar();
				}

			}
		
	}
	
	public void intentoFallido(Vista vista) {
		int numeroAleatorio = (int) (Math.random() * 4);
		int dificultad = vista.GRID_SIZE == 4 ? 0 : vista.GRID_SIZE == 5 ? 1 : 2;
		String mensajeAleatorio = vista.mensajesIntento[dificultad][numeroAleatorio];
		JOptionPane.showMessageDialog(vista, mensajeAleatorio);
		
	}
	
	public void limpiarLetras(JTextField[][] gridCells, Vista vista) {
		boolean mostrarAlertaLetra = false;
		for (int i = 0; i < vista.getGrid(); i++) {
			for (int j = 0; j < vista.getGrid(); j++) {
				boolean esLetra = isLetter(vista.gridCells[i][j].getText());
				if (esLetra) {
					vista.gridCells[i][j].setText("0");
					vista.getForeground();
					vista.gridCells[i][j].setBackground(Color.white);
					mostrarAlertaLetra = true;
				}
				vista.gridPanel.add(gridCells[i][j]);
			}
		}
	}
	private static boolean isLetter(String cadena) {
		try {
			Integer.parseInt(cadena);
			return false;
		} catch (NumberFormatException excepcion) {
			return true;
		}
	}
	
	public void reiniciarNivel(Vista vista) {
		vista.iniciar();
	}
	
	public void limpiarPanel(JTextField[][] gridCells, Vista vista) {
		for (int i = 0; i < vista.getGrid(); i++) {
			for (int j = 0; j < vista.getGrid(); j++) {
				vista.gridCells[i][j].setText("0");
				vista.getForeground();
				vista.gridCells[i][j].setBackground(Color.white);
				vista.gridPanel.add(gridCells[i][j]);
			}
		}
	}
	public void pintarGrilla(boolean[][] aPintar, JTextField[][] gridCells, Vista vista) {
		for (int i = 0; i < vista.getGrid(); i++) {
			for (int j = 0; j < vista.getGrid(); j++) {
				if (aPintar[i][j]) {
					vista.getForeground();
					gridCells[i][j].setBackground(Color.GREEN);

				} else {
					vista.getForeground();
					gridCells[i][j].setBackground(Color.white);
				}

				vista.gridPanel.add(gridCells[i][j]);
			}
		}
	}
	
	public void ganador(Vista vista) {
		timer.stop();
		Controlador.fin = System.currentTimeMillis();
		double tiempo = (double) ((Controlador.fin - Controlador.inicio) / 1000);
		if (Controlador.tiempoRecord > tiempo)
			Controlador.tiempoRecord = tiempo;
		devolverTiempo(tiempo);
		JOptionPane.showMessageDialog(vista,"¡Felicidades has ganado! Tiempo tardado: " + Controlador.minutos + ":" + Controlador.segundos + " minutos");
		vista.setVisible(false);
		Menu menu = new Menu();
		menu.setVisible(true);

	}

	public void devolverTiempo(double tiempo) {
		minutos = 0;
		while (tiempo > 60) {
			tiempo -= 60;
			minutos += 1;
		}
		segundos = (int) tiempo;
	}

}
