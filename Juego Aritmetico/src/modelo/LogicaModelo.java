package modelo;

import javax.swing.JTextField;

public class LogicaModelo {
	private int gridSize;
	private boolean[][] aPintar; 
	private static boolean contieneLetra;
	
	public boolean juegoResuelto(JTextField[][] gridCells, int[] sumaEsperadaFilas, int[] sumaEsperadaColumnas) {
		int longitud = gridCells.length;
		int longitud2 = gridCells[0].length;
		boolean igualdadTotal = true;
		aPintar = new boolean[gridSize][gridSize];
		
		for (int i = 0; i < gridCells[0].length; i++) {
			int sumaFila = 0;
			int sumaCol = 0;
			
			for (int j = 0; j < longitud; j++) {
				String value = gridCells[i][j].getText();
				if(esNumero(value)) {
					sumaFila += Integer.parseInt(value);
				}
			}
			igualdadTotal = igualdadTotal && sumaFila == sumaEsperadaFilas[i];
			
			if(sumaFila == sumaEsperadaFilas[i]) {
				for (int j = 0; j < longitud; j++) {
					this.aPintar[i][j] = true;
				}
			}else {
				for (int j = 0; j < longitud; j++) {
					this.aPintar[i][j] = false;
				}
			}			
			
			for (int j = 0; j < longitud2; j++) {
				String value = gridCells[j][i].getText();
				if(esNumero(value)) {
					sumaCol += Integer.parseInt(value);
				}
			}
			igualdadTotal = igualdadTotal && sumaCol == sumaEsperadaColumnas[i];
			
			if(sumaCol == sumaEsperadaColumnas[i]) {
				for (int j = 0; j < longitud2; j++) {
					this.aPintar[j][i] = true;
				}
			}else {
				for (int j = 0; j < longitud2; j++) {
					this.aPintar[j][i] = false;
				}
			}
		}
		return igualdadTotal;
	}
	
	private static boolean esNumero(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException excepcion) {
        	contieneLetra = true;
            return false;
        }
    }
	
	public void setGridSize(int size) {
		this.gridSize = size;
	}
	
	public boolean[][] getAPintar() {
		return this.aPintar;
	}
	
	public boolean contieneLetra() {
		return this.contieneLetra;
	}
}
